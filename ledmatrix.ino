//
//  ledmatrix.ino
//  ledmatrix
//
//  Copyright © 2016 GRAEMELYON. All rights reserved.
//

#include "Arduino.h"
#include "patterns.h"

// Constants
#define GRIDSZ 4
#define N_PATTERNS 6
#define R1 13
#define R2 12
#define R3 11
#define R4 10
#define C1 5
#define C2 3
#define C3 2
#define C4 4


// Prototypes
void allOff(void);
void displayGrid(byte p[GRIDSZ]);
void showPattern(const byte pattern[], size_t len, int n);


// Globals
int row[] = {R1, R2, R3, R4};
int col[] = {C1, C2, C3, C4};
int patSel = 0;
size_t pat_len;
unsigned long tstart;


void setup() {
    // Intialize pins
    // Order matters here as Due defaults to high on OUTPUT mode
    // and we dont want to source cuurent for > 1 LED on each row
    for (int i=0;i<GRIDSZ;i++) {
        pinMode(row[i], OUTPUT);
        digitalWrite(row[i], LOW);
        pinMode(col[i], OUTPUT);
        digitalWrite(col[i], LOW);
    }
}

void loop() {
    
    switch (patSel) {
        case 0:
            pat_len = sizeof(runnerTable) / sizeof(runnerTable[0]);
            showPattern(runnerTable, pat_len, 2);
            break;
        case 1:
            pat_len = sizeof(bars) / sizeof(bars[0]);
            showPattern(bars, pat_len, 3);
            break;
        case 2:
            pat_len = sizeof(spinner) / sizeof(spinner[0]);
            showPattern(spinner, pat_len, 3);
            break;
        case 3:
            pat_len = sizeof(box) / sizeof(box[0]);
            showPattern(box, pat_len, 2);
            break;
        case 4:
            pat_len = sizeof(viz) / sizeof(viz[0]);
            showPattern(viz, pat_len, 5);
            break;
        case 5:
            pat_len = sizeof(filler) / sizeof(filler[0]);
            showPattern(filler, pat_len, 3);
            break;
    }
    patSel = ++patSel % N_PATTERNS;
    
}


void showPattern(const byte pattern[], size_t len, int n) {
    
    byte patternBuf[GRIDSZ];
    byte duration;
    
    for (int i=0; i<n; i++) {
        int patIdx = 0;
        while (patIdx < len) {
            // Read 4 byte frame and duration
            memcpy_P(patternBuf, pattern + patIdx, GRIDSZ);
            duration = pgm_read_byte_near(pattern + patIdx + GRIDSZ);
            patIdx += GRIDSZ + 1;
            tstart = millis();
            while (millis()-tstart < duration) {
                displayGrid(patternBuf);
            }
        }
    }
}


void displayGrid(byte p[]) {
    // Displays a single frame
    // Logic here converts Bxxxx, Bxxxx, etc from columns to rows
    int n;
    for (int c=0; c<GRIDSZ; c++) {
        allOff();
        digitalWrite(col[c], HIGH); // activate column
        n = 0;
        for (int r=0; r<GRIDSZ; r++) {
            if (bitRead(p[n], GRIDSZ-1-c)==1) {
                digitalWrite(row[r], HIGH); // activate row
            } else {
                digitalWrite(row[r], LOW);
            }
            n++;
        }
        delay(1);
    }
}


void allOff() {
    for (int i=0; i<GRIDSZ; i++) {
        digitalWrite(row[i], LOW);
        digitalWrite(col[i], LOW);
    }
}



//EOF
